sudo apt-get -yqq update && \
sudo apt-get install -yq --no-install-recommends ca-certificates expat libgomp1 && \
sudo apt-get autoremove -y && \
sudo apt-get clean -y


export PKG_CONFIG_PATH=/home/camille/Project/ffmpeg/lib/pkgconfig
export LD_LIBRARY_PATH=/home/camille/Project/ffmpeg/lib
export PREFIX=//home/camille/Project/ffmpeg
export MAKEFLAGS="-j4"

export FFMPEG_VERSION=4.1  \
FDKAAC_VERSION=0.1.5       \
LAME_VERSION=3.99.5        \
LIBASS_VERSION=0.13.7      \
OGG_VERSION=1.3.2          \
OPENCOREAMR_VERSION=0.1.5  \
OPUS_VERSION=1.2           \
OPENJPEG_VERSION=2.1.2     \
THEORA_VERSION=1.1.1       \
VORBIS_VERSION=1.3.5       \
VPX_VERSION=1.7.0          \
X264_VERSION=20170226-2245-stable \
X265_VERSION=2.3           \
XVID_VERSION=1.3.4         \
FREETYPE_VERSION=2.5.5     \
FRIBIDI_VERSION=0.19.7     \
FONTCONFIG_VERSION=2.12.4  \
LIBVIDSTAB_VERSION=1.1.0   \
KVAZAAR_VERSION=1.2.0      \
BLURAY_VERSION=1.1.1       \
AOM_VERSION=master         \
CDIO_VERSION=2.0.0         \
CDIO_PARANOIA_VERSION=10.2 \
SRC=/usr/local

export OGG_SHA256SUM="e19ee34711d7af328cb26287f4137e70630e7261b17cbe3cd41011d73a654692  libogg-1.3.2.tar.gz"
export OPUS_SHA256SUM="77db45a87b51578fbc49555ef1b10926179861d854eb2613207dc79d9ec0a9a9  opus-1.2.tar.gz"
export VORBIS_SHA256SUM="6efbcecdd3e5dfbf090341b485da9d176eb250d893e3eb378c428a2db38301ce  libvorbis-1.3.5.tar.gz"
export THEORA_SHA256SUM="40952956c47811928d1e7922cda3bc1f427eb75680c3c37249c91e949054916b  libtheora-1.1.1.tar.gz"
export XVID_SHA256SUM="4e9fd62728885855bc5007fe1be58df42e5e274497591fec37249e1052ae316f  xvidcore-1.3.4.tar.gz"
export FREETYPE_SHA256SUM="5d03dd76c2171a7601e9ce10551d52d4471cf92cd205948e60289251daddffa8  freetype-2.5.5.tar.gz"
export LIBVIDSTAB_SHA256SUM="14d2a053e56edad4f397be0cb3ef8eb1ec3150404ce99a426c4eb641861dc0bb  v1.1.0.tar.gz"
export LIBASS_SHA256SUM="8fadf294bf701300d4605e6f1d92929304187fca4b8d8a47889315526adbafd7  0.13.7.tar.gz"
export FRIBIDI_SHA256SUM="3fc96fa9473bd31dcb5500bdf1aa78b337ba13eb8c301e7c28923fea982453a8  0.19.7.tar.gz"


 buildDeps="autoconf \
            automake \
            cmake \
            curl \
            bzip2 \
            libexpat1-dev \
            g++ \
            gcc \
            git \
            gperf \
            libtool \
            make \
            nasm \
            perl \
            pkg-config \
            python \
            libssl-dev \
            yasm \
            zlib1g-dev \
            text-info \
            help2man" && \
sudo apt-get -yqq update && \
sudo apt-get install -yq --no-install-recommends ${buildDeps}

DIR=/tmp/opencore-amr && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sL https://kent.dl.sourceforge.net/project/opencore-amr/opencore-amr/opencore-amr-${OPENCOREAMR_VERSION}.tar.gz | \
tar -zx --strip-components=1 && \
./configure --prefix="${PREFIX}" --disable-shared --enable-static  && \
make && \
make install && \
rm -rf ${DIR}

DIR=/tmp/x264 && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sL https://download.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-${X264_VERSION}.tar.bz2 | \
tar -jx --strip-components=1 && \
./configure --prefix="${PREFIX}" --disable-shared --enable-static --enable-pic --disable-cli && \
make && \
make install && \
rm -rf ${DIR}

DIR=/tmp/x265 && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sL https://download.videolan.org/pub/videolan/x265/x265_${X265_VERSION}.tar.gz  | \
tar -zx && \
cd x265_${X265_VERSION}/build/linux && \
sed -i "/-DEXTRA_LIB/ s/$/ -DCMAKE_INSTALL_PREFIX=\${PREFIX}/" multilib.sh && \
sed -i "/^cmake/ s/$/ -DENABLE_CLI=OFF/" multilib.sh && \
./multilib.sh && \
make -C 8bit install && \
rm -rf ${DIR}

DIR=/tmp/ogg && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO http://downloads.xiph.org/releases/ogg/libogg-${OGG_VERSION}.tar.gz && \
echo ${OGG_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f libogg-${OGG_VERSION}.tar.gz && \
./configure --prefix="${PREFIX}" --disable-shared --enable-static  && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/opus && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO https://archive.mozilla.org/pub/opus/opus-${OPUS_VERSION}.tar.gz && \
echo ${OPUS_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f opus-${OPUS_VERSION}.tar.gz && \
autoreconf -fiv && \
./configure --prefix="${PREFIX}" --disable-shared --enable-static && \
make && \
make install && \
rm -rf ${DIR}

DIR=/tmp/vorbis && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO http://downloads.xiph.org/releases/vorbis/libvorbis-${VORBIS_VERSION}.tar.gz && \
echo ${VORBIS_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f libvorbis-${VORBIS_VERSION}.tar.gz && \
./configure --prefix="${PREFIX}" --with-ogg="${PREFIX}" --disable-shared --enable-static && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/theora && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO http://downloads.xiph.org/releases/theora/libtheora-${THEORA_VERSION}.tar.gz && \
echo ${THEORA_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f libtheora-${THEORA_VERSION}.tar.gz && \
./configure --prefix="${PREFIX}" --with-ogg="${PREFIX}" --disable-shared --enable-static && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/vpx && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sL https://codeload.github.com/webmproject/libvpx/tar.gz/v${VPX_VERSION} | \
tar -zx --strip-components=1 && \
./configure --prefix="${PREFIX}" --enable-vp8 --enable-vp9 --enable-vp9-highbitdepth --enable-pic --disable-shared --enable-static \
--disable-debug --disable-examples --disable-docs --disable-install-bins  && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/lame && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sL https://kent.dl.sourceforge.net/project/lame/lame/$(echo ${LAME_VERSION} | sed -e 's/[^0-9]*\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)\([0-9A-Za-z-]*\)/\1.\2/')/lame-${LAME_VERSION}.tar.gz | \
tar -zx --strip-components=1 && \
./configure --prefix="${PREFIX}" --bindir="${PREFIX}/bin" --disable-shared --enable-static --enable-nasm --enable-pic --disable-frontend && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/xvid && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO http://downloads.xvid.org/downloads/xvidcore-${XVID_VERSION}.tar.gz && \
echo ${XVID_SHA256SUM} | sha256sum --check && \
tar -zx -f xvidcore-${XVID_VERSION}.tar.gz && \
cd xvidcore/build/generic && \
./configure --prefix="${PREFIX}" --bindir="${PREFIX}/bin" --datadir="${DIR}" && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/fdk-aac && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sL https://github.com/mstorsjo/fdk-aac/archive/v${FDKAAC_VERSION}.tar.gz | \
tar -zx --strip-components=1 && \
autoreconf -fiv && \
./configure --prefix="${PREFIX}" --disable-shared --enable-static --datadir="${DIR}" && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/openjpeg && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sL https://github.com/uclouvain/openjpeg/archive/v${OPENJPEG_VERSION}.tar.gz | \
tar -zx --strip-components=1 && \
cmake -DBUILD_THIRDPARTY:BOOL=ON -DCMAKE_INSTALL_PREFIX="${PREFIX}" -DBUILD_SHARED_LIBS=0 . && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/freetype && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO http://download.savannah.gnu.org/releases/freetype/freetype-${FREETYPE_VERSION}.tar.gz && \
echo ${FREETYPE_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f freetype-${FREETYPE_VERSION}.tar.gz && \
./configure --prefix="${PREFIX}" --disable-shared --enable-static && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/vid.stab && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO https://github.com/georgmartius/vid.stab/archive/v${LIBVIDSTAB_VERSION}.tar.gz &&\
echo ${LIBVIDSTAB_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f v${LIBVIDSTAB_VERSION}.tar.gz && \
cmake -DCMAKE_INSTALL_PREFIX="${PREFIX}" -DBUILD_SHARED_LIBS=0 . && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/fribidi && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO https://github.com/fribidi/fribidi/archive/${FRIBIDI_VERSION}.tar.gz && \
echo ${FRIBIDI_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f ${FRIBIDI_VERSION}.tar.gz && \
sed -i 's/^SUBDIRS =.*/SUBDIRS=gen.tab charset lib/' Makefile.am && \
./bootstrap --no-config && \
./configure -prefix="${PREFIX}" --disable-shared --enable-static && \
make -j 1 && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/fontconfig && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO https://www.freedesktop.org/software/fontconfig/release/fontconfig-${FONTCONFIG_VERSION}.tar.bz2 &&\
tar -jx --strip-components=1 -f fontconfig-${FONTCONFIG_VERSION}.tar.bz2 && \
./configure -prefix="${PREFIX}" --disable-shared --enable-static && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/libass && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO https://github.com/libass/libass/archive/${LIBASS_VERSION}.tar.gz &&\
echo ${LIBASS_SHA256SUM} | sha256sum --check && \
tar -zx --strip-components=1 -f ${LIBASS_VERSION}.tar.gz && \
./autogen.sh && \
./configure -prefix="${PREFIX}" --disable-shared --enable-static && \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/kvazaar && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO https://github.com/ultravideo/kvazaar/archive/v${KVAZAAR_VERSION}.tar.gz &&\
tar -zx --strip-components=1 -f v${KVAZAAR_VERSION}.tar.gz && \
./autogen.sh && \
./configure -prefix="${PREFIX}" --disable-shared --enable-static && \
make && \
sudo make install && \
rm -rf ${DIR}

dir=/tmp/aom ; \
mkdir -p ${dir} ; \
cd ${dir} ; \
curl -sLO https://aomedia.googlesource.com/aom/+archive/${AOM_VERSION}.tar.gz ; \
tar -zx -f ${AOM_VERSION}.tar.gz ; \
rm -rf CMakeCache.txt CMakeFiles ; \
mkdir -p ./aom_build ; \
cd ./aom_build ; \
cmake -DCMAKE_INSTALL_PREFIX="${PREFIX}" -DBUILD_SHARED_LIBS=0 ..; \
make ; \
sudo make install ; \
rm -rf ${dir}

DIR=/tmp/libcdio && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO http://git.savannah.gnu.org/cgit/libcdio.git/snapshot/libcdio-release-${CDIO_VERSION}.tar.gz; \
tar -zx -f ./libcdio-release-${CDIO_VERSION}.tar.gz ; \
cd libcdio-release-${CDIO_VERSION} && \
./autogen.sh && \
./configure -prefix="${PREFIX}" --disable-shared --enable-static --enable-maintainer-mode&& \
make && \
sudo make install && \
rm -rf ${DIR}

DIR=/tmp/libcdio_paranoia && \
mkdir -p ${DIR} && \
cd ${DIR} && \
curl -sLO https://github.com/rocky/libcdio-paranoia/archive/release-${CDIO_PARANOIA_VERSION}+${CDIO_VERSION}.tar.gz; \
tar -zx -f ./release-${CDIO_PARANOIA_VERSION}+${CDIO_VERSION}.tar.gz ; \
cd libcdio-paranoia-release-${CDIO_PARANOIA_VERSION}-${CDIO_VERSION} && \
./autogen.sh && \
./configure -prefix="${PREFIX}" --disable-shared --enable-static && \
make && \
sudo make install && \
rm -rf ${DIR}


DIR="/home/camille/Project/ffmpeg/build/ffmpeg" && mkdir ${DIR}; cd ${DIR} && \
curl -sLO https://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.bz2 && \
tar -jx --strip-components=1 -f ffmpeg-${FFMPEG_VERSION}.tar.bz2 && \
sudo make clean ;
./configure \
    --pkgconfigdir="/home/camille/Project/ffmpeg/lib/pkgconfig" \
    --pkg-config-flags="--static" \
    --enable-libxvid --extra-libs=-lm\
    --disable-debug \
    --disable-doc \
    --disable-ffplay \
    --enable-avresample \
    --enable-libopencore-amrnb \
    --enable-libopencore-amrwb \
    --enable-gpl \
    --enable-libass \
    --enable-libfreetype \
    --enable-libvidstab \
    --enable-libmp3lame \
    --enable-libopenjpeg \
    --enable-libopus \
    --enable-libtheora \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libaom --extra-libs=-lpthread \
    --enable-libx265 \
    --enable-libx264 \
    --enable-nonfree \
    --enable-openssl \
    --enable-libfdk_aac \
    --enable-libkvazaar \
    --enable-postproc \
    --enable-small \
    --enable-version3 \
    --extra-cflags="-I${PREFIX}/include -static" \
    --extra-ldflags="-L${PREFIX}/lib -static" \
    --extra-libs=-ldl \
    --disable-shared \
    --enable-static \
    --enable-avcodec  --extra-libs=-laom\
    --enable-libcdio \
    --enable-libfribidi \
    --enable-libopencv \
    --prefix="${PREFIX}"  && \
make && \
sudo make install && \
make distclean && \
hash -r && \
cd tools && \
make qt-faststart && \
cp qt-faststart ${PREFIX}/bin

## cleanup
ldd ${PREFIX}/bin/ffmpeg | grep opt/ffmpeg | cut -d ' ' -f 3 | xargs -i cp {} /usr/local/lib/ && \
cp ${PREFIX}/bin/* /usr/local/bin/ && \
cp -r ${PREFIX}/share/ffmpeg /usr/local/share/ && \
LD_LIBRARY_PATH=/usr/local/lib ffmpeg -buildconf


LD_LIBRARY_PATH=/usr/local/lib
